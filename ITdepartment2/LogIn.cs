﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data.SqlClient;
using BaseSql;
using System.IO;

namespace ITdepartment2
{
    public partial class LogIn : Form
    {
        private Workers workersForm;
        public LogIn()
        {
            //this.workersForm = workersForm;
            InitializeComponent();
        }


        private static string dbPath = Application.StartupPath + "\\db.sqlite";
        public string DbPath
        {
            get
            {
                return dbPath;
            }
           
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            txtPassword.Text = "Password";
            txtPassword.ForeColor = Color.Gray;
            txtPassword.TextAlign = HorizontalAlignment.Center;
            txtPassword.UseSystemPasswordChar = false;
            txtUserName.Text = "Username";
            txtUserName.ForeColor = Color.Gray;
            txtUserName.TextAlign = HorizontalAlignment.Center;
            try
            {
                this.LoadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            if (txtUserName.Text == "Username")
            {
                txtUserName.Text = "";
            }
        }


        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                txtUserName.Text = "Username";
                txtUserName.ForeColor = Color.Gray;
                txtUserName.TextAlign = HorizontalAlignment.Center;
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            if (txtPassword.Text == "Password")
            {
                txtPassword.Text = "";
                txtPassword.UseSystemPasswordChar = true;
            }


        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                txtPassword.Text = "Password";
                txtPassword.ForeColor = Color.Gray;
                txtPassword.TextAlign = HorizontalAlignment.Center;
            }
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {

            string user_id = txtUserName.Text;
            string user_password = txtPassword.Text;
            if (File.Exists(dbPath))
            {
                SQLiteConnection sqlConnection = new SQLiteConnection("Data Source=" + dbPath + "; Version=3; Compress=true;");
                string query = "SELECT user_id, user_password FROM authorization";
                SQLiteCommand sqlSelectCommand = new SQLiteCommand(query, sqlConnection);
                sqlConnection.Open();
                SQLiteDataReader result = sqlSelectCommand.ExecuteReader();
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        using (MD5 md5hashFunction = MD5.Create()){
                            string hash = this.GetMd5Hash(md5hashFunction, user_password.ToString());
                            if (user_id == result["user_id"].ToString() && hash == result["user_password"].ToString())
                            { 
                                if (sqlliteConnection.State == System.Data.ConnectionState.Open)
                                {
                                    sqlliteConnection.Close();
                                }
                                //sqlConnection.Close();
                                this.Hide();
                                var form2 = new Request(this);
                                form2.Closed += (s, args) => this.Close();
                                form2.Show();
                               // sqlConnection.Close();
                                    break;

                            }
                            else continue;
                        }

                    }
                    sqlConnection.Close();
                }

            }
            else
            {
                LoadData();
                this.btnLogIn_Click(sender, e);
            }
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public delegate DataTable GetData(string DataSource, string DataBase, string DataSourceID, string DataSourcePsw, string UserTable, string UserLoginField, string UserNameField, string User_Base_Dept, string UserPswField);

        private DataTable CopyFromServerToDB(string DataSource, string DataBase, string DataSourceID, string DataSourcePsw, string UserTable, string UserLoginField, string UserNameField, string User_Base_Dept, string UserPswField)
        {

            string SQLQuery = "SELECT " + UserLoginField + ", " + UserNameField + ", " + User_Base_Dept + ", " + UserPswField + ", dbo.orgDepartment.dept_name AS dept_name FROM " + UserTable + ", dbo.orgDepartment WHERE " + UserNameField + " NOT LIKE '%''%' AND (dbo.orgDepartment.dept_code = " + UserTable + "." + User_Base_Dept + " OR " + UserTable + "." + User_Base_Dept + " LIKE NULL);";
            DataTable data = new DataTable();
            try
            {
                SqlCommand query = new SqlCommand(SQLQuery, Sql.SqlConnect(DataSource, DataBase, DataSourceID, DataSourcePsw));
                SqlDataAdapter adapter = new SqlDataAdapter(query);
                adapter.Fill(data);
                return data;
            }
            catch (Exception ex)
            {
                MessageBox.Show("DataTable is empty" + ex.Message + "\n" + ex.StackTrace);
                return data;
            }
        }
        public SQLiteConnection sqlliteConnection = new SQLiteConnection("Data Source=" + dbPath + "; Version=3; Compress=true;");
        public void LoadData()
        {
            
            SQLiteCommand sqlCommand;
            SQLiteDataAdapter sqlDataAdapter;
            DataTable sqlDataTable;
            string dbPath = Application.StartupPath + "\\db.sqlite";
            try
            {
                if (!File.Exists(dbPath))
                {
                    //create database
                    SQLiteConnection.CreateFile(dbPath);
                    sqlliteConnection = new SQLiteConnection("Data Source=" + dbPath + "; Version=3; Compress=true;");
                    sqlDataTable = new DataTable();
                    sqlliteConnection.Open(); //open connection

                    string sqlCreateTableAuthorization = "CREATE TABLE authorization("+
                                                          "user_id varchar(30) NOT NULL,"+
                                                          "user_name nvarchar(50) NOT NULL,"+
                                                          "user_base_dept nvarchar(10), "+
                                                          "user_base_dept_name nvarchar(70) ,"+ 
                                                          "user_password varchar(100) NOT NULL);\n";
                    string sqlCreateTableRequest = "CREATE TABLE UZDWIT01 ("+
                                                    "requirenment    NVARCHAR (4)   NOT NULL PRIMARY KEY,"+
                                                    "bizPlaceCode    NVARCHAR (5)   NOT NULL,"+
                                                    "bizPlaceName NVARCHAR (50)  NOT NULL,"+
                                                    "requestDate     NVARCHAR (10)  NOT NULL, "+
                                                    "creatorDeptCode NVARCHAR (3)   NOT NULL, "+
                                                    "creatorDeptName NVARCHAR (50)  NOT NULL, "+
                                                    "creatorID       NVARCHAR (20)  NOT NULL, "+
                                                    "creatorName     NVARCHAR (50)  NOT NULL, "+
                                                    "empId           NVARCHAR (20)  NOT NULL, "+
                                                    "empName         NVARCHAR (20)  NOT NULL, "+ 
                                                    "empDeptCode     NVARCHAR (3)   NOT NULL, "+
                                                    "empDeptName     NVARCHAR (50)  NOT NULL, "+
                                                    "reqRemark       NVARCHAR (256), "+
                                                    "requestStatus   NVARCHAR (20)  NOT NULL);\n";

                    string sqlCreateTableItems = "CREATE TABLE UZDWIT01_ITEMS ("+
                                                    "itemName  NVARCHAR (60)  NOT NULL,"+
                                                    "itemNameFr          NVARCHAR (60)  NOT NULL,"+
                                                    "itemQuantity        INTEGER        NOT NULL, "+
                                                    "itemSpec        NVARCHAR (40),"+
                                                    "itemUnit            NVARCHAR (5)   NOT NULL,"+
                                                    "itemRemark          NVARCHAR (256),"+
                                                    "RequirenmentForItem NVARCHAR (20)  NOT NULL,"+
                                                    "FOREIGN KEY  (RequirenmentForItem)  REFERENCES UZDWIT01 (requirenment) );\n";
                    sqlCreateTableAuthorization += sqlCreateTableRequest;
                    sqlCreateTableAuthorization += sqlCreateTableItems;
                    sqlCommand = sqlliteConnection.CreateCommand();// create command
                    sqlCommand.CommandText = sqlCreateTableAuthorization; // command text is sqlCreateTableAuthorization command
                    sqlCommand.ExecuteNonQuery(); // execute query


                    GetData copyFromServerToDB = new GetData(CopyFromServerToDB); // function of copyng data from server
                    DataTable ServerDataTable = new DataTable();  //server data table

                    //using copyFromServerToDB function
                    ServerDataTable = copyFromServerToDB("192.168.7.248", "OPEN_TEST", "sa", "sapadmin1@", "dbo.sysUserMaster", "user_id", "user_name", "base_dept" , "password");


                    sqlDataAdapter = new SQLiteDataAdapter("SELECT user_id, user_name, user_base_dept, user_base_dept_name, user_password FROM authorization", sqlliteConnection);
                    sqlDataAdapter.AcceptChangesDuringFill = false;
                    sqlDataAdapter.Fill(sqlDataTable);


                    for (int i = 0; i < ServerDataTable.Rows.Count; i++)
                    {
                        var AddData = sqlDataTable.NewRow();
                        AddData["user_id"] = ServerDataTable.Rows[i]["user_id"];
                        AddData["user_name"] = ServerDataTable.Rows[i]["user_name"];
                        AddData["user_password"] = ServerDataTable.Rows[i]["password"];
                        AddData["user_base_dept"] = ServerDataTable.Rows[i]["base_dept"];
                        AddData["user_base_dept_name"] = ServerDataTable.Rows[i]["dept_name"];

                        sqlDataTable.Rows.Add(AddData["user_id"], AddData["user_name"], AddData["user_base_dept"],  AddData["user_base_dept_name"], AddData["user_password"]);
                    }


                    try
                    {
                        SQLiteDataAdapter InsertAdapter = new SQLiteDataAdapter("SELECT user_id, user_name, user_base_dept, user_base_dept_name, user_password FROM authorization", sqlliteConnection);

                        SQLiteCommandBuilder sqlCommandBuilder = new SQLiteCommandBuilder(InsertAdapter);
                        
                        InsertAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                        InsertAdapter.Update(sqlDataTable);

                        sqlliteConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
                    }
                    sqlliteConnection.Close();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("1." + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

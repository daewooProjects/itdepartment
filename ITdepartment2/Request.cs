﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;

namespace ITdepartment2
{
    public partial class Request : Form
    {

        private LogIn logIn;
        public Request(LogIn logIn)
        {
            this.logIn = logIn;
            InitializeComponent();
            DataRow newRow = bizPlace.NewRow();
            newRow["bizPlaceCode"] = "10";
            newRow["bizPlaceName"] = "Fergana Office";
            newRow["bizPlaceOutput"] = "10   |   Fergana Office";
            this.bizPlace.Rows.Add(newRow);
            DataRow newRow1 = bizPlace.NewRow();
            newRow1["bizPlaceCode"] = "20";
            newRow1["bizPlaceName"] = "Fergana";
            newRow1["bizPlaceOutput"] = "20   |   Fergana";
            this.bizPlace.Rows.Add(newRow1);
            DataRow newRow2 = bizPlace.NewRow(); 
            newRow2["bizPlaceCode"] = "30";
            newRow2["bizPlaceName"] = "Tashlak";
            newRow2["bizPlaceOutput"] = "30   |   Tashlak";
            this.bizPlace.Rows.Add(newRow2);
            DataRow newRow3 = bizPlace.NewRow();
            newRow3["bizPlaceCode"] = "40";
            newRow3["bizPlaceName"] = "Kumtepa";
            newRow3["bizPlaceOutput"] = "40   |   Kumtepa";
            this.bizPlace.Rows.Add(newRow3);
            DataRow newRow4 = bizPlace.NewRow();
            newRow4["bizPlaceCode"] = "50";
            newRow4["bizPlaceName"] = "Bukhara Office";
            newRow4["bizPlaceOutput"] = "50   |   Bukhara Office";
            this.bizPlace.Rows.Add(newRow4);
            DataRow newRow5 = bizPlace.NewRow();
            newRow5["bizPlaceCode"] = "60";
            newRow5["bizPlaceName"] = "Bukhara";
            newRow5["bizPlaceOutput"] = "60   |   Bukhara";
            this.bizPlace.Rows.Add(newRow5);
            this.txtHeadDate.Properties.MaxValue = new DateTime(DateTime.Now.Year);
           
            
        }
        public int check_point = 0;
        public string temp1;
        public string temp2;
        private DataTable tableRequest = new DataTable();
        private DataTable tableItems = new DataTable();
        private static LogIn login = new LogIn();
        private bool editValue = true;
        SQLiteConnection sqlliteConnection = new SQLiteConnection("Data Source=" + login.DbPath + "; Version=3; Compress=true;");

        private void Request_Load(object sender, EventArgs e)
        {
            if (!File.Exists(logIn.DbPath))
            {
                logIn.LoadData();
            }
            //bindingSource1.DataSource = 
            SQLiteDataAdapter sqlDataAdapter;
            
            if (sqlliteConnection.State == System.Data.ConnectionState.Closed)
            {
                sqlliteConnection.Open();
            }

            string requestQuery="SELECT * FROM UZDWIT01;";
            sqlDataAdapter = new SQLiteDataAdapter(requestQuery, sqlliteConnection);
            sqlDataAdapter.Fill(tableRequest);
            
            
            MessageBox.Show(tableItems.Columns.Count.ToString() + "-"+ tableItems.Rows.Count.ToString());
            dataGridRequest.AutoGenerateColumns = false;
            dataGridRequest.DataSource = tableRequest;
           
            sqlliteConnectionClose();
            }

        public void sqlliteConnectionOpen()
        {
            if (sqlliteConnection.State == System.Data.ConnectionState.Closed)
            {
                sqlliteConnection.Open();
            }
        }
        public void sqlliteConnectionClose()
        {
            if (sqlliteConnection.State == System.Data.ConnectionState.Open)
            {
                sqlliteConnection.Close();
            } 
        }
        public void ExecuteCreateQuery(string query, SQLiteConnection connection)
        {
            DataTable sqlDataTable = new DataTable();
            SQLiteCommand sqlCommand;
            sqlCommand = connection.CreateCommand();// create command
            sqlCommand.CommandText = query;        // command text is sqlCreateTableAuthorization command
            sqlCommand.ExecuteNonQuery();         // execute query
            SQLiteDataAdapter sqlDataAdapter = new SQLiteDataAdapter(query, connection);
            SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(sqlDataAdapter);

            sqlDataAdapter.InsertCommand = commandBuilder.GetInsertCommand();
         
        }

        private void OpenFormWorkers()
        {
            Workers workers = new Workers(this, logIn);
            workers.Show();
            //txtEmpCode.Text = temp1;
           // txtEmpName.Text = temp2;
        }
        private void txtEmpCode_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            check_point = 0;
            OpenFormWorkers();
        }

        private void txtDateStart_EditValueChanged(object sender, EventArgs e)
        {
            DateTime temp = new DateTime(txtDateEnd.DateTime.Year, txtDateEnd.DateTime.Month, txtDateEnd.DateTime.Day);
            if (txtDateEnd.Text != "")
            {
                   if(txtDateStart.DateTime.Year >= temp.Year)
                       if(txtDateStart.DateTime.Month >= temp.Month)
                           if (txtDateStart.DateTime.Day > temp.Day)
                           {
                               txtDateStart.EditValue = temp;
                           }
            }

            if (txtDateEnd.Text == "")
            {
                DateTime temp2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (txtDateStart.DateTime.Year >= temp2.Year)
                    if (txtDateStart.DateTime.Month >= temp2.Month)
                        if (txtDateStart.DateTime.Day > temp2.Day)
                        {
                            txtDateStart.EditValue = temp2;
                        } 
            }
        }

        private void txtDateEnd_EditValueChanged(object sender, EventArgs e)
        {
            DateTime temp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            if (txtDateEnd.DateTime.Year >= temp.Year)
                if (txtDateEnd.DateTime.Month >= temp.Month)
                    if (txtDateEnd.DateTime.Day > temp.Day)
                    {
                        txtDateEnd.EditValue = temp;
                    }
            
            if (txtDateStart.Text != "")
            {
                DateTime temp2 = new DateTime(txtDateStart.DateTime.Year, txtDateStart.DateTime.Month, txtDateStart.DateTime.Day);
                if (txtDateStart.DateTime.Year >= txtDateEnd.DateTime.Year)
                    if (txtDateStart.DateTime.Month >= txtDateEnd.DateTime.Month)
                        if (txtDateStart.DateTime.Day > txtDateEnd.DateTime.Day)
                        {
                            txtDateEnd.EditValue = temp2;
                        }
            }

        }

        private void dataGridRequest_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                    txtHeadDocN.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["DocNo"].Value.ToString();
                    txtHeadStatus.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["State"].Value.ToString();
                    txtHeadDate.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["RequestDate"].Value.ToString();
                    txtHeadBizPlace.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["BusinessPlace"].Value.ToString();

                    txtHeadEmpName.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["EmpName"].Value.ToString();
                    txtHeadEmpCode.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["empID"].Value.ToString();
                    txtHeadEmpDepart.Text = txtHeadCreatorDept.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["empDeptCode"].Value.ToString();

                    txtHeadCreatorDept.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["creatorDeptCode"].Value.ToString();
                    txtHeadCreatorName.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["CreateEmp"].Value.ToString();
                    txtHeadCreatorCode.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["creatorID"].Value.ToString();
                    
                    txtHeadRemark.Text = dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["Remark"].Value.ToString();

                    sqlliteConnectionOpen();
                    tableItems.Clear();
                    dataGridItems.Refresh();
                    string itemsQuery = "SELECT * FROM UZDWIT01_ITEMS WHERE RequirenmentForItem LIKE '%" + dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["DocNo"].Value.ToString() + "%'; AND RequirenmentForItem NOT LIKE NULL";
                    SQLiteDataAdapter sqlItemsDataAdapter = new SQLiteDataAdapter(itemsQuery, sqlliteConnection);
                    sqlItemsDataAdapter.Fill(tableItems);

                    dataGridItems.AutoGenerateColumns = false;
                    dataGridItems.DataSource = tableItems;
                    sqlliteConnectionClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            txtHeadDocN.Text = "";
            txtHeadStatus.Text = "Request";
            txtHeadDate.Text = "";
            txtHeadDate.Text = "";
            txtHeadBizPlace.Text = "";
            
            txtHeadEmpName.Text = "";
            txtHeadEmpCode.Text = "";
            txtHeadEmpDepart.Text = "";

            txtHeadCreatorDept.Text = "";
            txtHeadCreatorName.Text = "";
            txtHeadCreatorCode.Text = "";

            txtHeadRemark.Text = "";


            DataTable AddingItems = new DataTable();
            DataColumn itemName = AddingItems.Columns.Add("itemName", typeof(String));
            DataColumn itemNameFr = AddingItems.Columns.Add("itemNameFr", typeof(String));
            DataColumn itemQuantity = AddingItems.Columns.Add("itemQuantity", typeof(String));
            DataColumn itemSpec = AddingItems.Columns.Add("itemSpec", typeof(String));
            DataColumn itemUnit = AddingItems.Columns.Add("itemUnit", typeof(String));
            DataColumn itemRemark = AddingItems.Columns.Add("itemRemark", typeof(String));
            DataColumn RequirenmentForItem = AddingItems.Columns.Add("RequirenmentForItem", typeof(String));
            dataGridItems.DataSource = AddingItems;
            editValue = false;
           // dataGridItems.Rows.Clear();
        }

        private void contextMenuStrip1_Opened(object sender, EventArgs e)
        {
            if (dataGridRequest.Rows.Count > 0 && dataGridRequest.SelectedRows.Count > 0)
            {
                conEdit.Enabled = true;
                conDelete.Enabled = true;
                conPreview.Enabled = true;
                conPrint.Enabled = true;
            }
        }

        private void txtHeadEmpCode_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            check_point = 1;
            OpenFormWorkers();
        }

        private void txtHeadCreatorCode_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            check_point = 2;
            OpenFormWorkers();
        }

        private void txtHeadDate_EditValueChanged(object sender, EventArgs e)
        {
            DateTime temp2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            if (txtDateStart.DateTime.Year >= temp2.Year)
                if (txtDateStart.DateTime.Month >= temp2.Month)
                    if (txtDateStart.DateTime.Day > temp2.Day)
                    {
                        txtDateStart.EditValue = temp2;
                    } 
        }

        private void AddNewRequest()
        {
            sqlliteConnectionOpen();

            var AddData = tableRequest.NewRow();
            AddData["requirenment"] = txtHeadDocN.Text;
            AddData["bizPlaceCode"] = txtHeadBizPlace.ValueMember;
            AddData["bizPlaceName"] = txtHeadBizPlace.ValueMember;
            AddData["requestDate"] = txtHeadDate.Text;
            AddData["creatorDeptCode"] = txtHeadCreatorDept.Text;
            AddData["creatorDeptName"] = txtHeadCreatorDept.Text;
            AddData["creatorID"] = txtHeadCreatorCode.Text;
            AddData["creatorName"] = txtHeadCreatorName.Text;
            AddData["empID"] = txtHeadEmpCode.Text;
            AddData["empName"] = txtHeadEmpName.Text;
            AddData["empDeptCode"] = txtHeadEmpDepart.Text;
            AddData["empDeptName"] = txtHeadEmpDepart.Text;
            AddData["reqRemark"] = txtHeadRemark.Text;
            AddData["requestStatus"] = txtHeadStatus.Text;
            tableRequest.Rows.Add(AddData["requirenment"],
                                  AddData["bizPlaceCode"],
                                  AddData["bizPlaceName"],
                                  AddData["requestDate"],
                                  AddData["creatorDeptCode"],
                                  AddData["creatorDeptName"],
                                  AddData["creatorID"],
                                  AddData["creatorName"],
                                  AddData["empID"],
                                  AddData["empName"],
                                  AddData["empDeptCode"],
                                  AddData["empDeptName"],
                                  AddData["reqRemark"],
                                  AddData["requestStatus"]);

            try
            {
                SQLiteDataAdapter InsertAdapter = new SQLiteDataAdapter("SELECT requirenment, bizPlaceCode, bizPlaceName, requestDate, creatorDeptCode, creatorDeptName, creatorID, creatorName, empId, empName, empDeptCode, empDeptName, reqRemark, requestStatus FROM UZDWIT01;", sqlliteConnection);
                SQLiteCommandBuilder sqlCommandBuilder = new SQLiteCommandBuilder(InsertAdapter);
                InsertAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                InsertAdapter.Update(tableRequest);
                sqlliteConnectionClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AddNewItem()
        {
            sqlliteConnectionOpen();
            try
            {
                int index = dataGridItems.Rows.Count - 1;
              /*  for (int i = 0; i < index; i++)
                {
                    MessageBox.Show((dataGridItems.Rows.Count).ToString());
                    var AddData2 = tableItems.NewRow();
                    AddData2["itemName"] = dataGridItems.Rows[i].Cells["ItemName"].Value.ToString();
                    AddData2["itemNameFr"] = dataGridItems.Rows[i].Cells["ItemNameFr"].Value.ToString();
                    AddData2["itemQuantity"] = dataGridItems.Rows[i].Cells["Quantity"].Value.ToString();
                    AddData2["itemSpec"] = dataGridItems.Rows[i].Cells["Spec"].Value.ToString();
                    AddData2["itemUnit"] = dataGridItems.Rows[i].Cells["Unit"].Value.ToString();
                    AddData2["itemRemark"] = dataGridItems.Rows[i].Cells["RemarkForItem"].Value.ToString();
                    AddData2["RequirenmentForItem"] = tableRequest.Rows[tableRequest.Rows.Count - 1]["requirenment"].ToString();
                    tableItems.Rows.Add(AddData2["itemName"],
                                        AddData2["itemNameFr"],
                                        AddData2["itemQuantity"],
                                        AddData2["itemSpec"],
                                        AddData2["itemUnit"],
                                        AddData2["itemRemark"],
                                        AddData2["RequirenmentForItem"]);
                    MessageBox.Show(tableItems.Rows.Count.ToString());
                }*/
                
                SQLiteDataAdapter InsertItemsAdapter = new SQLiteDataAdapter(" SELECT itemName, itemNameFr, itemQuantity, itemSpec, itemUnit, itemRemark, RequirenmentForItem FROM UZDWIT01_ITEMS;", sqlliteConnection);
                SQLiteCommandBuilder sqlItemsCommandBuilder = new SQLiteCommandBuilder(InsertItemsAdapter);
                InsertItemsAdapter.InsertCommand = sqlItemsCommandBuilder.GetInsertCommand();
                InsertItemsAdapter.Update(tableItems);
                sqlliteConnectionClose();
                dataGridItems.DataSource = tableItems;
                dataGridItems.AllowUserToAddRows = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }


        private void EditRequest()
        {
            try
            {
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("requirenment", txtHeadDocN.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("bizPlaceCode", txtHeadBizPlace.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("bizPlaceName", txtHeadBizPlace.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("requestDate", txtHeadDate.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("creatorDeptCode", txtHeadCreatorDept.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("creatorDeptName", txtHeadCreatorDept.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("creatorID", txtHeadCreatorCode.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("creatorName", txtHeadCreatorName.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("empID", txtHeadEmpCode.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("empName", txtHeadEmpName.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("empDeptCode", txtHeadEmpDepart.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("empDeptName", txtHeadEmpDepart.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("reqRemark", txtHeadRemark.Text);
                tableRequest.Rows[dataGridRequest.CurrentCell.RowIndex].SetField("requestStatus", txtHeadStatus.Text);

                SQLiteDataAdapter AlterRequestAdapter = new SQLiteDataAdapter("SELECT requirenment, bizPlaceCode, bizPlaceName, requestDate, creatorDeptCode, creatorDeptName, creatorID, creatorName, empId, empName, empDeptCode, empDeptName, reqRemark, requestStatus FROM UZDWIT01;", sqlliteConnection);
                SQLiteCommandBuilder sqlItemsCommandBuilder = new SQLiteCommandBuilder(AlterRequestAdapter);
                AlterRequestAdapter.UpdateCommand = sqlItemsCommandBuilder.GetUpdateCommand();
                AlterRequestAdapter.Update(tableRequest);
                sqlliteConnectionClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void EditItem()
        {
            try
            {
                for (int i = 0; i < dataGridItems.Rows.Count - 1; i++)
                {
                    try
                    {
                        tableItems.Rows[i].SetField("itemName", dataGridItems.Rows[i].Cells["ItemName"].Value.ToString());
                        tableItems.Rows[i].SetField("itemNameFr", dataGridItems.Rows[i].Cells["ItemNameFr"].Value.ToString());
                        tableItems.Rows[i].SetField("itemQuantity", dataGridItems.Rows[i].Cells["Quantity"].Value.ToString());
                        tableItems.Rows[i].SetField("itemSpec", dataGridItems.Rows[i].Cells["Spec"].Value.ToString());
                        tableItems.Rows[i].SetField("itemUnit", dataGridItems.Rows[i].Cells["Unit"].Value.ToString());
                        tableItems.Rows[i].SetField("itemRemark", dataGridItems.Rows[i].Cells["RemarkForItem"].Value.ToString());
                        tableItems.Rows[i].SetField("RequirenmentForItem", dataGridRequest.Rows[i].Cells["requirenment"].Value.ToString());

                        MessageBox.Show(tableItems.Rows.Count.ToString());
                    }
                    catch (Exception ex2)
                    {
                        MessageBox.Show(ex2.Message + "\n" + ex2.StackTrace);
                    }

                   
                }
                 SQLiteDataAdapter AlterItemsAdapter = new SQLiteDataAdapter(" SELECT itemName, itemNameFr, itemQuantity, itemSpec, itemUnit, itemRemark, RequirenmentForItem FROM UZDWIT01_ITEMS WHERE RequirenmentForItem LIKE '%" + dataGridRequest.Rows[dataGridRequest.CurrentCell.RowIndex].Cells["DocNo"].Value.ToString() + "%'; AND RequirenmentForItem NOT LIKE NULL", sqlliteConnection);
                    SQLiteCommandBuilder sqlItemsCommandBuilder = new SQLiteCommandBuilder(AlterItemsAdapter);
                    AlterItemsAdapter.UpdateCommand = sqlItemsCommandBuilder.GetUpdateCommand();
                    AlterItemsAdapter.Update(tableItems);
                    dataGridItems.DataSource = tableItems;
                    dataGridItems.AllowUserToAddRows = false;
                sqlliteConnectionClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

       private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (editValue == true)
            {
                MessageBox.Show("EditRequest");
                EditRequest();
                if (dataGridItems.Rows.Count == 2)
                {
                    MessageBox.Show("AddNewItem");
                    AddNewItem();
                }
                else
                {
                    MessageBox.Show("EditItem");
                    EditItem();
                }
            }
            else
            {
                MessageBox.Show("AddNewRequest");
                AddNewRequest();
                if (dataGridItems.Rows.Count > 0)
                {
                    MessageBox.Show("AddNewItem");
                    AddNewItem();
                }
            }
        }

        private void dataGridItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            dataGridItems.AllowUserToAddRows = true;
        }
            
   
          
        }
    }   

//                                    _____________________
//                                   |\\\\\\\\\\|//////////|
//                                   | \\\\\\\\\|///////// |
//                                   |  \\\\\\\\|////////  |
//                                   |   \\\\\\\|///////   |
//                                   |    \\\\\\|//////    |
//                                   |     \\\\\|/////     |
//                                   |______\\\\|////______|
//                                          ¯¯¯¯¯¯¯¯¯       
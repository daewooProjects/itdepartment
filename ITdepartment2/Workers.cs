﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data.SqlClient;

namespace ITdepartment2
{
    public partial class Workers : Form
    {
        private Request requestForm;
        private LogIn logIn;
        private int increment = 0;
        public Workers(Request requestForm, LogIn logIn)
        {
            this.requestForm = requestForm;
            InitializeComponent();
            this.logIn = logIn;
            try
            {
                        
                SQLiteConnection sqlConnection = new SQLiteConnection("Data Source=" + logIn.DbPath + "; Version=3; Compress=true;");
                string query = "SELECT user_id, user_name, user_base_dept FROM authorization WHERE user_base_dept LIKE '%OIC%'";
                //SQLiteCommand sqlSelectCommand = new SQLiteCommand(query, sqlConnection);
                sqlConnection.Open();
                //SQLiteDataReader result = sqlSelectCommand.ExecuteReader();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, sqlConnection);
                adapter.Fill(tableEmp);
                
                dataGridEmp.ClearSelection();
                txtEmpCode.Text = "";
                txtEmpName.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void queryForWorkers(string searchValueEmpCode, string searchValueEmpName)
        {
            try
            {
                tableEmp.Clear();
                SQLiteConnection sqlConnection = new SQLiteConnection("Data Source=" + logIn.DbPath + "; Version=3; Compress=true;");
                string query = "SELECT user_id, user_name, user_base_dept FROM authorization WHERE user_base_dept LIKE '%OIC%' AND user_id LIKE '%" + searchValueEmpCode + "%' AND user_name LIKE '%" + searchValueEmpName + "%'";
                //SQLiteCommand sqlSelectCommand = new SQLiteCommand(query, sqlConnection);
                sqlConnection.Open();
                //SQLiteDataReader result = sqlSelectCommand.ExecuteReader();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, sqlConnection);
                adapter.Fill(tableEmp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void queryForWorkers(string searchValueEmp)
        {
            try
            {
                tableEmp.Clear();
                SQLiteConnection sqlConnection = new SQLiteConnection("Data Source=" + logIn.DbPath + "; Version=3; Compress=true;");
                string query = "SELECT user_id, user_name, user_base_dept FROM authorization WHERE user_base_dept LIKE '%OIC%' AND (user_id LIKE '%" + searchValueEmp + "%' OR user_name LIKE '%" + searchValueEmp + "%')";
                //SQLiteCommand sqlSelectCommand = new SQLiteCommand(query, sqlConnection);
                sqlConnection.Open();
                //SQLiteDataReader result = sqlSelectCommand.ExecuteReader();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, sqlConnection);
                adapter.Fill(tableEmp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }
        private string department = "";
        public void SetData()
        {
            if (dataGridEmp.SelectedCells.Count > 0 && increment>1)
            {
                int selectedrowindex = dataGridEmp.CurrentCell.RowIndex;
                int selectedcolumnindex = dataGridEmp.CurrentCell.ColumnIndex;
                string a = dataGridEmp.Rows[selectedrowindex].Cells[selectedcolumnindex].Value.ToString();
                txtEmpCode.Text = dataGridEmp.Rows[selectedrowindex].Cells[0].Value.ToString();
                txtEmpName.Text = dataGridEmp.Rows[selectedrowindex].Cells[1].Value.ToString();
                department = dataGridEmp.Rows[selectedrowindex].Cells[2].Value.ToString();
            }
        }
        private void dataGridEmp_SelectionChanged(object sender, EventArgs e)
        {
            increment++;
            SetData();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (txtEmpCode.Text != "" && txtEmpName.Text != "")
            {
                switch (requestForm.check_point) { 
                    case 0:
                        requestForm.txtEmpCode.Text = txtEmpCode.Text;
                        requestForm.txtEmpName.Text = txtEmpName.Text;
                        break;
                    case 1: 
                        requestForm.txtHeadEmpCode.Text = txtEmpCode.Text;
                        requestForm.txtHeadEmpName.Text = txtEmpName.Text;
                        requestForm.txtHeadEmpDepart.Text = department;
                        break;
                    case 2:
                        requestForm.txtHeadCreatorCode.Text = txtEmpCode.Text;
                        requestForm.txtHeadCreatorName.Text = txtEmpName.Text;
                        requestForm.txtHeadCreatorDept.Text = department;
                        break;
                   // default: break;
              
            
            }
                this.Close();
            }
            else MessageBox.Show("Not filled");
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            if (txtEmpCode.Text != "" && txtEmpName.Text != "")
            {
                queryForWorkers(txtEmpCode.Text, txtEmpName.Text);

            }
            else if (txtEmpName.Text == "" && txtEmpCode.Text != "")
            {
                queryForWorkers(txtEmpCode.Text);
            }
            else if (txtEmpName.Text != "" && txtEmpCode.Text == "")
            {
                queryForWorkers(txtEmpName.Text);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Workers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F)
            {
                search();
            }
        }


    }
}

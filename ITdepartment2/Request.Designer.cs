﻿using System.Data;
namespace ITdepartment2
{
    partial class Request
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtBizPlace = new System.Windows.Forms.ComboBox();
            this.dataSet1 = new System.Data.DataSet();
            this.bizPlace = new System.Data.DataTable();
            this.bizPlaceCode = new System.Data.DataColumn();
            this.bizPlaceName = new System.Data.DataColumn();
            this.dataColumn1 = new System.Data.DataColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.txtDateEnd = new DevExpress.XtraEditors.DateEdit();
            this.txtDateStart = new DevExpress.XtraEditors.DateEdit();
            this.labDateEnd = new System.Windows.Forms.Label();
            this.labDateStart = new System.Windows.Forms.Label();
            this.labEmployer = new System.Windows.Forms.Label();
            this.txtEmpName = new DevExpress.XtraEditors.ButtonEdit();
            this.labBizPlace = new System.Windows.Forms.Label();
            this.txtEmpCode = new DevExpress.XtraEditors.ButtonEdit();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelButtom = new System.Windows.Forms.Panel();
            this.dataGridItems = new System.Windows.Forms.DataGridView();
            this.LineNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNameFr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Spec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkForItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteAll = new System.Windows.Forms.ToolStripMenuItem();
            this.Print = new System.Windows.Forms.ToolStripMenuItem();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.dataGridRequest = new System.Windows.Forms.DataGridView();
            this.RequestDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BusinessPlace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBizPlaceCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatorDeptCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatorDeptName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatorID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empDeptCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empDeptName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateEmp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.conNew = new System.Windows.Forms.ToolStripMenuItem();
            this.conEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.conDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.conPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.conPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.panelRight = new System.Windows.Forms.Panel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtHeadDate = new DevExpress.XtraEditors.DateEdit();
            this.txtHeadBizPlace = new System.Windows.Forms.ComboBox();
            this.txtHeadStatus = new System.Windows.Forms.TextBox();
            this.txtHeadRemark = new DevExpress.XtraEditors.MemoEdit();
            this.txtHeadDocN = new System.Windows.Forms.TextBox();
            this.txtHeadCreatorDept = new System.Windows.Forms.TextBox();
            this.txtHeadCreatorName = new DevExpress.XtraEditors.ButtonEdit();
            this.txtHeadCreatorCode = new DevExpress.XtraEditors.ButtonEdit();
            this.txtHeadEmpDepart = new System.Windows.Forms.TextBox();
            this.txtHeadEmpName = new DevExpress.XtraEditors.ButtonEdit();
            this.txtHeadEmpCode = new DevExpress.XtraEditors.ButtonEdit();
            this.labRemarkHeader = new System.Windows.Forms.Label();
            this.labDepartmentCreatHeader = new System.Windows.Forms.Label();
            this.labCreatorHeader = new System.Windows.Forms.Label();
            this.labDepartmentEmpHeader = new System.Windows.Forms.Label();
            this.labEmployerHeader = new System.Windows.Forms.Label();
            this.labDateHeader = new System.Windows.Forms.Label();
            this.labStatus = new System.Windows.Forms.Label();
            this.labDocN = new System.Windows.Forms.Label();
            this.labBizPlaceHeader = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bizPlace)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpCode.Properties)).BeginInit();
            this.panelButtom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridItems)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRequest)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadCreatorName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadCreatorCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadEmpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTop.Controls.Add(this.txtBizPlace);
            this.panelTop.Controls.Add(this.toolStrip1);
            this.panelTop.Controls.Add(this.txtDateEnd);
            this.panelTop.Controls.Add(this.txtDateStart);
            this.panelTop.Controls.Add(this.labDateEnd);
            this.panelTop.Controls.Add(this.labDateStart);
            this.panelTop.Controls.Add(this.labEmployer);
            this.panelTop.Controls.Add(this.txtEmpName);
            this.panelTop.Controls.Add(this.labBizPlace);
            this.panelTop.Controls.Add(this.txtEmpCode);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1285, 137);
            this.panelTop.TabIndex = 0;
            // 
            // txtBizPlace
            // 
            this.txtBizPlace.DataSource = this.dataSet1;
            this.txtBizPlace.DisplayMember = "bizPlace.bizPlaceOutput";
            this.txtBizPlace.FormattingEnabled = true;
            this.txtBizPlace.Location = new System.Drawing.Point(108, 48);
            this.txtBizPlace.Name = "txtBizPlace";
            this.txtBizPlace.Size = new System.Drawing.Size(169, 23);
            this.txtBizPlace.TabIndex = 11;
            this.txtBizPlace.ValueMember = "bizPlace.bizPlaceCode";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.bizPlace});
            // 
            // bizPlace
            // 
            this.bizPlace.Columns.AddRange(new System.Data.DataColumn[] {
            this.bizPlaceCode,
            this.bizPlaceName,
            this.dataColumn1});
            this.bizPlace.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "bizPlaceCode"}, true)});
            this.bizPlace.PrimaryKey = new System.Data.DataColumn[] {
        this.bizPlaceCode};
            this.bizPlace.TableName = "bizPlace";
            // 
            // bizPlaceCode
            // 
            this.bizPlaceCode.AllowDBNull = false;
            this.bizPlaceCode.Caption = "bizPlaceCode";
            this.bizPlaceCode.ColumnName = "bizPlaceCode";
            // 
            // bizPlaceName
            // 
            this.bizPlaceName.Caption = "bizPlaceName";
            this.bizPlaceName.ColumnName = "bizPlaceName";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "bizPlaceOutput";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.GhostWhite;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripButton7,
            this.toolStripButton8});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1281, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Margin = new System.Windows.Forms.Padding(30, 1, 10, 2);
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripButton1.Size = new System.Drawing.Size(54, 22);
            this.toolStripButton1.Text = "Look up";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(35, 22);
            this.toolStripButton2.Text = "New";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(108, 22);
            this.toolStripButton3.Text = "Add Row/Add line";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(136, 22);
            this.toolStripButton4.Text = "Delete Row/ Delete Line";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(35, 22);
            this.toolStripButton5.Text = "Save";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(44, 22);
            this.toolStripButton6.Text = "Delete";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton7.Text = "Preview";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 22);
            this.toolStripButton8.Text = "Print";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // txtDateEnd
            // 
            this.txtDateEnd.EditValue = null;
            this.txtDateEnd.Location = new System.Drawing.Point(693, 54);
            this.txtDateEnd.Name = "txtDateEnd";
            this.txtDateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDateEnd.Size = new System.Drawing.Size(135, 20);
            this.txtDateEnd.TabIndex = 8;
            this.txtDateEnd.EditValueChanged += new System.EventHandler(this.txtDateEnd_EditValueChanged);
            // 
            // txtDateStart
            // 
            this.txtDateStart.EditValue = null;
            this.txtDateStart.Location = new System.Drawing.Point(523, 54);
            this.txtDateStart.Name = "txtDateStart";
            this.txtDateStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDateStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDateStart.Size = new System.Drawing.Size(135, 20);
            this.txtDateStart.TabIndex = 7;
            this.txtDateStart.EditValueChanged += new System.EventHandler(this.txtDateStart_EditValueChanged);
            // 
            // labDateEnd
            // 
            this.labDateEnd.AutoSize = true;
            this.labDateEnd.Location = new System.Drawing.Point(673, 55);
            this.labDateEnd.Name = "labDateEnd";
            this.labDateEnd.Size = new System.Drawing.Size(14, 15);
            this.labDateEnd.TabIndex = 6;
            this.labDateEnd.Text = "~";
            // 
            // labDateStart
            // 
            this.labDateStart.AutoSize = true;
            this.labDateStart.Location = new System.Drawing.Point(476, 55);
            this.labDateStart.Name = "labDateStart";
            this.labDateStart.Size = new System.Drawing.Size(33, 15);
            this.labDateStart.TabIndex = 5;
            this.labDateStart.Text = "Date";
            // 
            // labEmployer
            // 
            this.labEmployer.AutoSize = true;
            this.labEmployer.Location = new System.Drawing.Point(43, 88);
            this.labEmployer.Name = "labEmployer";
            this.labEmployer.Size = new System.Drawing.Size(59, 15);
            this.labEmployer.TabIndex = 3;
            this.labEmployer.Text = "Employer";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(285, 87);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtEmpName.Size = new System.Drawing.Size(200, 20);
            this.txtEmpName.TabIndex = 2;
            this.txtEmpName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtEmpCode_ButtonClick);
            // 
            // labBizPlace
            // 
            this.labBizPlace.AutoSize = true;
            this.labBizPlace.Location = new System.Drawing.Point(11, 51);
            this.labBizPlace.Name = "labBizPlace";
            this.labBizPlace.Size = new System.Drawing.Size(91, 15);
            this.labBizPlace.TabIndex = 1;
            this.labBizPlace.Text = "Business Place";
            // 
            // txtEmpCode
            // 
            this.txtEmpCode.EditValue = "";
            this.txtEmpCode.Location = new System.Drawing.Point(108, 87);
            this.txtEmpCode.Name = "txtEmpCode";
            this.txtEmpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtEmpCode.Properties.UseReadOnlyAppearance = false;
            this.txtEmpCode.Size = new System.Drawing.Size(171, 20);
            this.txtEmpCode.TabIndex = 0;
            this.txtEmpCode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtEmpCode_ButtonClick);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 137);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1285, 3);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panelButtom
            // 
            this.panelButtom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelButtom.Controls.Add(this.dataGridItems);
            this.panelButtom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtom.Location = new System.Drawing.Point(0, 487);
            this.panelButtom.Name = "panelButtom";
            this.panelButtom.Size = new System.Drawing.Size(1285, 196);
            this.panelButtom.TabIndex = 2;
            // 
            // dataGridItems
            // 
            this.dataGridItems.AllowUserToAddRows = false;
            this.dataGridItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LineNo,
            this.ItemName,
            this.ItemNameFr,
            this.Spec,
            this.Unit,
            this.Quantity,
            this.RemarkForItem});
            this.dataGridItems.ContextMenuStrip = this.contextMenuStrip2;
            this.dataGridItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridItems.Location = new System.Drawing.Point(0, 0);
            this.dataGridItems.Name = "dataGridItems";
            this.dataGridItems.Size = new System.Drawing.Size(1281, 192);
            this.dataGridItems.TabIndex = 0;
            this.dataGridItems.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridItems_CellContentClick);
            // 
            // LineNo
            // 
            this.LineNo.HeaderText = "Line №";
            this.LineNo.Name = "LineNo";
            this.LineNo.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "itemName";
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            // 
            // ItemNameFr
            // 
            this.ItemNameFr.DataPropertyName = "itemNameFr";
            this.ItemNameFr.HeaderText = "Item Name (Ru)";
            this.ItemNameFr.Name = "ItemNameFr";
            // 
            // Spec
            // 
            this.Spec.DataPropertyName = "itemSpec";
            this.Spec.HeaderText = "Spec";
            this.Spec.Name = "Spec";
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "itemUnit";
            this.Unit.HeaderText = "Unit";
            this.Unit.Name = "Unit";
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "itemQuantity";
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            // 
            // RemarkForItem
            // 
            this.RemarkForItem.DataPropertyName = "itemRemark";
            this.RemarkForItem.HeaderText = "Remark for Item";
            this.RemarkForItem.Name = "RemarkForItem";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddRow,
            this.DeleteRow,
            this.DeleteAll,
            this.Print});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(134, 92);
            // 
            // AddRow
            // 
            this.AddRow.Name = "AddRow";
            this.AddRow.Size = new System.Drawing.Size(133, 22);
            this.AddRow.Text = "Add Row";
            // 
            // DeleteRow
            // 
            this.DeleteRow.Name = "DeleteRow";
            this.DeleteRow.Size = new System.Drawing.Size(133, 22);
            this.DeleteRow.Text = "Delete Row";
            // 
            // DeleteAll
            // 
            this.DeleteAll.Name = "DeleteAll";
            this.DeleteAll.Size = new System.Drawing.Size(133, 22);
            this.DeleteAll.Text = "Delete";
            // 
            // Print
            // 
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(133, 22);
            this.Print.Text = "Print";
            // 
            // panelLeft
            // 
            this.panelLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelLeft.Controls.Add(this.dataGridRequest);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 140);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(625, 347);
            this.panelLeft.TabIndex = 3;
            // 
            // dataGridRequest
            // 
            this.dataGridRequest.AllowUserToAddRows = false;
            this.dataGridRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRequest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RequestDate,
            this.EmpName,
            this.Remark,
            this.BusinessPlace,
            this.colBizPlaceCode,
            this.creatorDeptCode,
            this.CreatorDeptName,
            this.creatorID,
            this.empId,
            this.empDeptCode,
            this.empDeptName,
            this.CreateEmp,
            this.DocNo,
            this.State});
            this.dataGridRequest.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridRequest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridRequest.Location = new System.Drawing.Point(0, 0);
            this.dataGridRequest.Name = "dataGridRequest";
            this.dataGridRequest.Size = new System.Drawing.Size(621, 343);
            this.dataGridRequest.TabIndex = 0;
            this.dataGridRequest.SelectionChanged += new System.EventHandler(this.dataGridRequest_SelectionChanged);
            // 
            // RequestDate
            // 
            this.RequestDate.DataPropertyName = "requestDate";
            this.RequestDate.HeaderText = "Request Date";
            this.RequestDate.Name = "RequestDate";
            // 
            // EmpName
            // 
            this.EmpName.DataPropertyName = "empName";
            this.EmpName.HeaderText = "EmpName";
            this.EmpName.Name = "EmpName";
            // 
            // Remark
            // 
            this.Remark.DataPropertyName = "reqRemark";
            this.Remark.HeaderText = "Remark";
            this.Remark.Name = "Remark";
            // 
            // BusinessPlace
            // 
            this.BusinessPlace.DataPropertyName = "bizPlaceName";
            this.BusinessPlace.HeaderText = "Business Place";
            this.BusinessPlace.Name = "BusinessPlace";
            // 
            // colBizPlaceCode
            // 
            this.colBizPlaceCode.DataPropertyName = "bizPlaceCode";
            this.colBizPlaceCode.HeaderText = "Business Place Code";
            this.colBizPlaceCode.Name = "colBizPlaceCode";
            this.colBizPlaceCode.Visible = false;
            // 
            // creatorDeptCode
            // 
            this.creatorDeptCode.DataPropertyName = "creatorDeptCode";
            this.creatorDeptCode.HeaderText = "Creator Dept Code";
            this.creatorDeptCode.Name = "creatorDeptCode";
            this.creatorDeptCode.Visible = false;
            // 
            // CreatorDeptName
            // 
            this.CreatorDeptName.DataPropertyName = "creatorDeptName";
            this.CreatorDeptName.HeaderText = "Creator Dept Name";
            this.CreatorDeptName.Name = "CreatorDeptName";
            this.CreatorDeptName.Visible = false;
            // 
            // creatorID
            // 
            this.creatorID.DataPropertyName = "creatorID";
            this.creatorID.HeaderText = "creatorID";
            this.creatorID.Name = "creatorID";
            // 
            // empId
            // 
            this.empId.DataPropertyName = "empId";
            this.empId.HeaderText = "empId";
            this.empId.Name = "empId";
            this.empId.Visible = false;
            // 
            // empDeptCode
            // 
            this.empDeptCode.DataPropertyName = "empDeptCode";
            this.empDeptCode.HeaderText = "empDeptCode";
            this.empDeptCode.Name = "empDeptCode";
            this.empDeptCode.Visible = false;
            // 
            // empDeptName
            // 
            this.empDeptName.DataPropertyName = "empDeptName";
            this.empDeptName.HeaderText = "empDeptName";
            this.empDeptName.Name = "empDeptName";
            this.empDeptName.Visible = false;
            // 
            // CreateEmp
            // 
            this.CreateEmp.DataPropertyName = "creatorName";
            this.CreateEmp.HeaderText = "Creator";
            this.CreateEmp.Name = "CreateEmp";
            // 
            // DocNo
            // 
            this.DocNo.DataPropertyName = "requirenment";
            this.DocNo.HeaderText = "Doc №";
            this.DocNo.Name = "DocNo";
            // 
            // State
            // 
            this.State.DataPropertyName = "requestStatus";
            this.State.HeaderText = "State";
            this.State.Name = "State";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.conNew,
            this.conEdit,
            this.conDelete,
            this.conPreview,
            this.conPrint});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(116, 114);
            this.contextMenuStrip1.Opened += new System.EventHandler(this.contextMenuStrip1_Opened);
            // 
            // conNew
            // 
            this.conNew.Name = "conNew";
            this.conNew.Size = new System.Drawing.Size(115, 22);
            this.conNew.Text = "New";
            this.conNew.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // conEdit
            // 
            this.conEdit.Enabled = false;
            this.conEdit.Name = "conEdit";
            this.conEdit.Size = new System.Drawing.Size(115, 22);
            this.conEdit.Text = "Edit";
            // 
            // conDelete
            // 
            this.conDelete.Enabled = false;
            this.conDelete.Name = "conDelete";
            this.conDelete.Size = new System.Drawing.Size(115, 22);
            this.conDelete.Text = "Delete";
            // 
            // conPreview
            // 
            this.conPreview.Enabled = false;
            this.conPreview.Name = "conPreview";
            this.conPreview.Size = new System.Drawing.Size(115, 22);
            this.conPreview.Text = "Preview";
            // 
            // conPrint
            // 
            this.conPrint.Enabled = false;
            this.conPrint.Name = "conPrint";
            this.conPrint.Size = new System.Drawing.Size(115, 22);
            this.conPrint.Text = "Print";
            // 
            // panelRight
            // 
            this.panelRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelRight.Controls.Add(this.groupControl1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(625, 140);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(660, 347);
            this.panelRight.TabIndex = 4;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtHeadDate);
            this.groupControl1.Controls.Add(this.txtHeadBizPlace);
            this.groupControl1.Controls.Add(this.txtHeadStatus);
            this.groupControl1.Controls.Add(this.txtHeadRemark);
            this.groupControl1.Controls.Add(this.txtHeadDocN);
            this.groupControl1.Controls.Add(this.txtHeadCreatorDept);
            this.groupControl1.Controls.Add(this.txtHeadCreatorName);
            this.groupControl1.Controls.Add(this.txtHeadCreatorCode);
            this.groupControl1.Controls.Add(this.txtHeadEmpDepart);
            this.groupControl1.Controls.Add(this.txtHeadEmpName);
            this.groupControl1.Controls.Add(this.txtHeadEmpCode);
            this.groupControl1.Controls.Add(this.labRemarkHeader);
            this.groupControl1.Controls.Add(this.labDepartmentCreatHeader);
            this.groupControl1.Controls.Add(this.labCreatorHeader);
            this.groupControl1.Controls.Add(this.labDepartmentEmpHeader);
            this.groupControl1.Controls.Add(this.labEmployerHeader);
            this.groupControl1.Controls.Add(this.labDateHeader);
            this.groupControl1.Controls.Add(this.labStatus);
            this.groupControl1.Controls.Add(this.labDocN);
            this.groupControl1.Controls.Add(this.labBizPlaceHeader);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(656, 343);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Header Information";
            // 
            // txtHeadDate
            // 
            this.txtHeadDate.EditValue = null;
            this.txtHeadDate.Location = new System.Drawing.Point(406, 74);
            this.txtHeadDate.Name = "txtHeadDate";
            this.txtHeadDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtHeadDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtHeadDate.Size = new System.Drawing.Size(135, 20);
            this.txtHeadDate.TabIndex = 22;
            this.txtHeadDate.EditValueChanged += new System.EventHandler(this.txtHeadDate_EditValueChanged);
            // 
            // txtHeadBizPlace
            // 
            this.txtHeadBizPlace.DataSource = this.dataSet1;
            this.txtHeadBizPlace.DisplayMember = "bizPlace.bizPlaceOutput";
            this.txtHeadBizPlace.FormattingEnabled = true;
            this.txtHeadBizPlace.Location = new System.Drawing.Point(144, 73);
            this.txtHeadBizPlace.Name = "txtHeadBizPlace";
            this.txtHeadBizPlace.Size = new System.Drawing.Size(135, 21);
            this.txtHeadBizPlace.TabIndex = 21;
            this.txtHeadBizPlace.ValueMember = "bizPlace.bizPlaceCode";
            // 
            // txtHeadStatus
            // 
            this.txtHeadStatus.Location = new System.Drawing.Point(406, 41);
            this.txtHeadStatus.Name = "txtHeadStatus";
            this.txtHeadStatus.ReadOnly = true;
            this.txtHeadStatus.Size = new System.Drawing.Size(135, 21);
            this.txtHeadStatus.TabIndex = 19;
            this.txtHeadStatus.Text = "Request";
            // 
            // txtHeadRemark
            // 
            this.txtHeadRemark.Location = new System.Drawing.Point(145, 262);
            this.txtHeadRemark.Name = "txtHeadRemark";
            this.txtHeadRemark.Size = new System.Drawing.Size(396, 64);
            this.txtHeadRemark.TabIndex = 18;
            // 
            // txtHeadDocN
            // 
            this.txtHeadDocN.Location = new System.Drawing.Point(144, 38);
            this.txtHeadDocN.Name = "txtHeadDocN";
            this.txtHeadDocN.Size = new System.Drawing.Size(135, 21);
            this.txtHeadDocN.TabIndex = 17;
            // 
            // txtHeadCreatorDept
            // 
            this.txtHeadCreatorDept.Location = new System.Drawing.Point(144, 220);
            this.txtHeadCreatorDept.Name = "txtHeadCreatorDept";
            this.txtHeadCreatorDept.Size = new System.Drawing.Size(397, 21);
            this.txtHeadCreatorDept.TabIndex = 16;
            // 
            // txtHeadCreatorName
            // 
            this.txtHeadCreatorName.Location = new System.Drawing.Point(285, 181);
            this.txtHeadCreatorName.Name = "txtHeadCreatorName";
            this.txtHeadCreatorName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtHeadCreatorName.Size = new System.Drawing.Size(256, 20);
            this.txtHeadCreatorName.TabIndex = 15;
            this.txtHeadCreatorName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtHeadCreatorCode_ButtonClick);
            // 
            // txtHeadCreatorCode
            // 
            this.txtHeadCreatorCode.Location = new System.Drawing.Point(145, 181);
            this.txtHeadCreatorCode.Name = "txtHeadCreatorCode";
            this.txtHeadCreatorCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtHeadCreatorCode.Size = new System.Drawing.Size(135, 20);
            this.txtHeadCreatorCode.TabIndex = 14;
            this.txtHeadCreatorCode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtHeadCreatorCode_ButtonClick);
            // 
            // txtHeadEmpDepart
            // 
            this.txtHeadEmpDepart.Location = new System.Drawing.Point(144, 144);
            this.txtHeadEmpDepart.Name = "txtHeadEmpDepart";
            this.txtHeadEmpDepart.Size = new System.Drawing.Size(397, 21);
            this.txtHeadEmpDepart.TabIndex = 13;
            // 
            // txtHeadEmpName
            // 
            this.txtHeadEmpName.Location = new System.Drawing.Point(285, 109);
            this.txtHeadEmpName.Name = "txtHeadEmpName";
            this.txtHeadEmpName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtHeadEmpName.Size = new System.Drawing.Size(256, 20);
            this.txtHeadEmpName.TabIndex = 12;
            this.txtHeadEmpName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtHeadEmpCode_ButtonClick);
            // 
            // txtHeadEmpCode
            // 
            this.txtHeadEmpCode.Location = new System.Drawing.Point(144, 108);
            this.txtHeadEmpCode.Name = "txtHeadEmpCode";
            this.txtHeadEmpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtHeadEmpCode.Size = new System.Drawing.Size(135, 20);
            this.txtHeadEmpCode.TabIndex = 11;
            this.txtHeadEmpCode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtHeadEmpCode_ButtonClick);
            // 
            // labRemarkHeader
            // 
            this.labRemarkHeader.AutoSize = true;
            this.labRemarkHeader.Location = new System.Drawing.Point(89, 264);
            this.labRemarkHeader.Name = "labRemarkHeader";
            this.labRemarkHeader.Size = new System.Drawing.Size(43, 13);
            this.labRemarkHeader.TabIndex = 9;
            this.labRemarkHeader.Text = "Remark";
            // 
            // labDepartmentCreatHeader
            // 
            this.labDepartmentCreatHeader.AutoSize = true;
            this.labDepartmentCreatHeader.Location = new System.Drawing.Point(60, 220);
            this.labDepartmentCreatHeader.Name = "labDepartmentCreatHeader";
            this.labDepartmentCreatHeader.Size = new System.Drawing.Size(64, 13);
            this.labDepartmentCreatHeader.TabIndex = 8;
            this.labDepartmentCreatHeader.Text = "Department";
            // 
            // labCreatorHeader
            // 
            this.labCreatorHeader.AutoSize = true;
            this.labCreatorHeader.Location = new System.Drawing.Point(80, 188);
            this.labCreatorHeader.Name = "labCreatorHeader";
            this.labCreatorHeader.Size = new System.Drawing.Size(44, 13);
            this.labCreatorHeader.TabIndex = 7;
            this.labCreatorHeader.Text = "Creator";
            // 
            // labDepartmentEmpHeader
            // 
            this.labDepartmentEmpHeader.AutoSize = true;
            this.labDepartmentEmpHeader.Location = new System.Drawing.Point(60, 152);
            this.labDepartmentEmpHeader.Name = "labDepartmentEmpHeader";
            this.labDepartmentEmpHeader.Size = new System.Drawing.Size(64, 13);
            this.labDepartmentEmpHeader.TabIndex = 6;
            this.labDepartmentEmpHeader.Text = "Department";
            // 
            // labEmployerHeader
            // 
            this.labEmployerHeader.AutoSize = true;
            this.labEmployerHeader.Location = new System.Drawing.Point(73, 111);
            this.labEmployerHeader.Name = "labEmployerHeader";
            this.labEmployerHeader.Size = new System.Drawing.Size(51, 13);
            this.labEmployerHeader.TabIndex = 5;
            this.labEmployerHeader.Text = "Employer";
            // 
            // labDateHeader
            // 
            this.labDateHeader.AutoSize = true;
            this.labDateHeader.Location = new System.Drawing.Point(343, 77);
            this.labDateHeader.Name = "labDateHeader";
            this.labDateHeader.Size = new System.Drawing.Size(30, 13);
            this.labDateHeader.TabIndex = 4;
            this.labDateHeader.Text = "Date";
            // 
            // labStatus
            // 
            this.labStatus.AutoSize = true;
            this.labStatus.Location = new System.Drawing.Point(340, 46);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(38, 13);
            this.labStatus.TabIndex = 3;
            this.labStatus.Text = "Status";
            // 
            // labDocN
            // 
            this.labDocN.AutoSize = true;
            this.labDocN.Location = new System.Drawing.Point(83, 41);
            this.labDocN.Name = "labDocN";
            this.labDocN.Size = new System.Drawing.Size(41, 13);
            this.labDocN.TabIndex = 2;
            this.labDocN.Text = "Doc №";
            // 
            // labBizPlaceHeader
            // 
            this.labBizPlaceHeader.AutoSize = true;
            this.labBizPlaceHeader.Location = new System.Drawing.Point(48, 77);
            this.labBizPlaceHeader.Name = "labBizPlaceHeader";
            this.labBizPlaceHeader.Size = new System.Drawing.Size(76, 13);
            this.labBizPlaceHeader.TabIndex = 1;
            this.labBizPlaceHeader.Text = "Business Place";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(625, 484);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(660, 3);
            this.splitter2.TabIndex = 5;
            this.splitter2.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(625, 140);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(3, 344);
            this.splitter3.TabIndex = 6;
            this.splitter3.TabStop = false;
            // 
            // Request
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1285, 683);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelButtom);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Name = "Request";
            this.Text = "Request";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Request_Load);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bizPlace)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpCode.Properties)).EndInit();
            this.panelButtom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridItems)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRequest)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadCreatorName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadCreatorCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeadEmpCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelButtom;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Label labEmployer;
        private System.Windows.Forms.Label labBizPlace;
        private DevExpress.XtraEditors.DateEdit txtDateEnd;
        private DevExpress.XtraEditors.DateEdit txtDateStart;
        private System.Windows.Forms.Label labDateEnd;
        private System.Windows.Forms.Label labDateStart;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.DataGridView dataGridRequest;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label labDepartmentEmpHeader;
        private System.Windows.Forms.Label labEmployerHeader;
        private System.Windows.Forms.Label labDateHeader;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.Label labDocN;
        private System.Windows.Forms.Label labBizPlaceHeader;
        private DevExpress.XtraEditors.MemoEdit txtHeadRemark;
        private System.Windows.Forms.TextBox txtHeadDocN;
        private System.Windows.Forms.Label labRemarkHeader;
        private System.Windows.Forms.Label labDepartmentCreatHeader;
        private System.Windows.Forms.Label labCreatorHeader;
        private System.Windows.Forms.TextBox txtHeadStatus;
        private System.Windows.Forms.DataGridView dataGridItems;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable bizPlace;
        private System.Data.DataColumn bizPlaceCode;
        private System.Data.DataColumn bizPlaceName;
        private System.Windows.Forms.ComboBox txtBizPlace;
        private DataColumn dataColumn1;
        internal DevExpress.XtraEditors.ButtonEdit txtEmpName;
        public DevExpress.XtraEditors.ButtonEdit txtEmpCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewTextBoxColumn BusinessPlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBizPlaceCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn creatorDeptCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatorDeptName;
        private System.Windows.Forms.DataGridViewTextBoxColumn creatorID;
        private System.Windows.Forms.DataGridViewTextBoxColumn empId;
        private System.Windows.Forms.DataGridViewTextBoxColumn empDeptCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn empDeptName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateEmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.ComboBox txtHeadBizPlace;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem conNew;
        private System.Windows.Forms.ToolStripMenuItem conEdit;
        private System.Windows.Forms.ToolStripMenuItem conDelete;
        private System.Windows.Forms.ToolStripMenuItem conPreview;
        private System.Windows.Forms.ToolStripMenuItem conPrint;
        public DevExpress.XtraEditors.ButtonEdit txtHeadCreatorName;
        public DevExpress.XtraEditors.ButtonEdit txtHeadCreatorCode;
        public DevExpress.XtraEditors.ButtonEdit txtHeadEmpName;
        public DevExpress.XtraEditors.ButtonEdit txtHeadEmpCode;
        private DevExpress.XtraEditors.DateEdit txtHeadDate;
        public System.Windows.Forms.TextBox txtHeadCreatorDept;
        public System.Windows.Forms.TextBox txtHeadEmpDepart;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNameFr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Spec;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn RemarkForItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem AddRow;
        private System.Windows.Forms.ToolStripMenuItem DeleteRow;
        private System.Windows.Forms.ToolStripMenuItem DeleteAll;
        private System.Windows.Forms.ToolStripMenuItem Print;
    }
}
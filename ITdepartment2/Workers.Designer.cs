﻿namespace ITdepartment2
{
    partial class Workers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.dataGridEmp = new System.Windows.Forms.DataGridView();
            this.colUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userbasedeptDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataSet1 = new System.Data.DataSet();
            this.tableEmp = new System.Data.DataTable();
            this.UserId = new System.Data.DataColumn();
            this.UserName = new System.Data.DataColumn();
            this.BaseDept = new System.Data.DataColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labEmpCode = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.txtEmpCode = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmp)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 324);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(444, 61);
            this.panel1.TabIndex = 0;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(306, 26);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(50, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(235, 26);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(50, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // dataGridEmp
            // 
            this.dataGridEmp.AutoGenerateColumns = false;
            this.dataGridEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUserId,
            this.usernameDataGridViewTextBoxColumn,
            this.userbasedeptDataGridViewTextBoxColumn});
            this.dataGridEmp.DataMember = "tableEmp";
            this.dataGridEmp.DataSource = this.dataSet1;
            this.dataGridEmp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridEmp.Location = new System.Drawing.Point(0, 0);
            this.dataGridEmp.MultiSelect = false;
            this.dataGridEmp.Name = "dataGridEmp";
            this.dataGridEmp.ReadOnly = true;
            this.dataGridEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridEmp.Size = new System.Drawing.Size(444, 247);
            this.dataGridEmp.TabIndex = 1;
            this.dataGridEmp.SelectionChanged += new System.EventHandler(this.dataGridEmp_SelectionChanged);
            this.dataGridEmp.DoubleClick += new System.EventHandler(this.buttonOk_Click);
            // 
            // colUserId
            // 
            this.colUserId.DataPropertyName = "user_id";
            this.colUserId.HeaderText = "Employer ID";
            this.colUserId.Name = "colUserId";
            this.colUserId.ReadOnly = true;
            // 
            // usernameDataGridViewTextBoxColumn
            // 
            this.usernameDataGridViewTextBoxColumn.DataPropertyName = "user_name";
            this.usernameDataGridViewTextBoxColumn.HeaderText = "Employer Name";
            this.usernameDataGridViewTextBoxColumn.MinimumWidth = 15;
            this.usernameDataGridViewTextBoxColumn.Name = "usernameDataGridViewTextBoxColumn";
            this.usernameDataGridViewTextBoxColumn.ReadOnly = true;
            this.usernameDataGridViewTextBoxColumn.Width = 200;
            // 
            // userbasedeptDataGridViewTextBoxColumn
            // 
            this.userbasedeptDataGridViewTextBoxColumn.DataPropertyName = "user_base_dept";
            this.userbasedeptDataGridViewTextBoxColumn.HeaderText = "Department";
            this.userbasedeptDataGridViewTextBoxColumn.Name = "userbasedeptDataGridViewTextBoxColumn";
            this.userbasedeptDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.tableEmp});
            // 
            // tableEmp
            // 
            this.tableEmp.Columns.AddRange(new System.Data.DataColumn[] {
            this.UserId,
            this.UserName,
            this.BaseDept});
            this.tableEmp.TableName = "tableEmp";
            // 
            // UserId
            // 
            this.UserId.ColumnName = "user_id";
            // 
            // UserName
            // 
            this.UserName.ColumnName = "user_name";
            // 
            // BaseDept
            // 
            this.BaseDept.ColumnName = "user_base_dept";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonSearch);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.labEmpCode);
            this.panel2.Controls.Add(this.txtEmpName);
            this.panel2.Controls.Add(this.txtEmpCode);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(444, 77);
            this.panel2.TabIndex = 2;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(366, 16);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Employer name";
            // 
            // labEmpCode
            // 
            this.labEmpCode.AutoSize = true;
            this.labEmpCode.Location = new System.Drawing.Point(6, 16);
            this.labEmpCode.Name = "labEmpCode";
            this.labEmpCode.Size = new System.Drawing.Size(78, 13);
            this.labEmpCode.TabIndex = 2;
            this.labEmpCode.Text = "Employer Code";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(90, 39);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(235, 20);
            this.txtEmpName.TabIndex = 1;
            // 
            // txtEmpCode
            // 
            this.txtEmpCode.Location = new System.Drawing.Point(90, 13);
            this.txtEmpCode.Name = "txtEmpCode";
            this.txtEmpCode.Size = new System.Drawing.Size(235, 20);
            this.txtEmpCode.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridEmp);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 77);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(444, 247);
            this.panel3.TabIndex = 3;
            // 
            // Workers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 385);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Workers";
            this.Text = "Workers";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Workers_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmp)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridEmp;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labEmpCode;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.TextBox txtEmpCode;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable tableEmp;
        private System.Data.DataColumn UserId;
        private System.Data.DataColumn UserName;
        private System.Data.DataColumn BaseDept;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserId;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userbasedeptDataGridViewTextBoxColumn;
    }
}